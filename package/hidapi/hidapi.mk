#############################################################
#
# hidapi
#
#############################################################
HIDAPI_VERSION = 0.7.0
HIDAPI_SITE = https://github.com/downloads/signal11/hidapi
HIDAPI_SOURCE = hidapi-$(HIDAPI_VERSION).zip 
HIDAPI_LICENSE = BSD or GPL
HIDAPI_LICENSE_FILES = LICENSE.txt
HIDAPI_DEPENDENCIES = libusb libiconv 
HIDAPI_INSTALL_STAGING = YES

define HIDAPI_EXTRACT_CMDS
 	unzip -d $(BUILD_DIR) $(DL_DIR)/$(HIDAPI_SOURCE)
endef

define HIDAPI_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" LD="$(TARGET_LD)" -C $(@D)/linux libhidapi-libusb.so 
endef

define HIDAPI_INSTALL_STAGING_CMDS
	$(INSTALL) -D -m 0644 $(@D)/hidapi/hidapi.h $(STAGING_DIR)/usr/include/hidapi/hidapi.h
endef

define HIDAPI_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/linux/libhidapi-libusb.so* $(TARGET_DIR)/usr/lib
endef

$(eval $(generic-package))
